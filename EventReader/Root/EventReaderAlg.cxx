// PhotonTruthMatchingTool includes
#include "EventReader/EventReaderAlg.h"
#include <xAODEventInfo/EventInfo.h>

// CaloCell
#include "CaloEvent/CaloCell.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloEvent/CaloClusterCellLink.h"
#include "CaloEvent/CaloClusterCellLinkContainer.h"
// #include "LArRawChannelContainer/"
#include "CaloEvent/CaloCompactCell.h"
#include "CaloEvent/CaloCompactCellContainer.h"

// Readout lib's
// #include "LArCabling/LArOnOffIdMapping.h"
// #include "TileConditions/TileCablingService.h"



// #include <xAODJet/JetContainer.h>

EventReaderAlg::EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator ) : 
    AthAlgorithm( name, pSvcLocator )
    , m_ntsvc("THistSvc/THistSvc", name)
    // , m_tileCabling("TileCablingSvc",name) //initialize Tile cabling service
    , m_larCablingKey("LArOnOffIdMap")
    , m_onlineLArID(0)
    , m_tileHWID(nullptr)
    , m_tileID(nullptr)
    , m_tileCabling(nullptr)
    , m_calocell_id(nullptr)
    // , m_ntsvc("THistSvc/THistSvc", name)
    {

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("LArOnOffMap",m_larCablingKey," LAr cabling map for online and offline.");

}

EventReaderAlg::~EventReaderAlg() {
  // delete m_runNumber;
  // delete m_bcid;
  // delete m_eventNumber;
}

StatusCode EventReaderAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  ATH_CHECK( m_tileCablingSvc.retrieve() );  
  m_tileCabling = m_tileCablingSvc->cablingService(); // initialize the cabling service to a pointer.
  ATH_CHECK( detStore()->retrieve(m_onlineLArID, "LArOnlineID") ); // get online ID info from LAr (online)  
  ATH_CHECK( detStore()->retrieve(m_tileHWID) ); // get hardware ID info from TileCal (online)
  ATH_CHECK( detStore()->retrieve(m_tileID) ); // get cell ID info from TileCal (offline)
  ATH_CHECK( detStore()->retrieve (m_calocell_id, "CaloCell_ID") );
  ATH_CHECK( m_larCablingKey.initialize());
  

  // Book the variables to save in the *.root 
  m_Tree = new TTree("data", "data");
  bookBranches(m_Tree);
  if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [data]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  clear();

  SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
  const LArOnOffIdMapping* larCabling=*larCablingHdl;

  const xAOD::EventInfo *eventInfo = nullptr;
  const xAOD::CaloClusterContainer* cls = nullptr;
  const xAOD::JetContainer* jets = nullptr;
  

  // // EventInfo
  if(!dumpEventInfo(eventInfo)) ATH_MSG_DEBUG("Event information cannot be collected!");
  //  // Jets  
  if(!dumpJetsInfo(jets,pr_jetName)) ATH_MSG_DEBUG("Jets information cannot be collected!");
  // // Clusters  
  if(!dumpClusterInfo(cls,pr_clusterName, larCabling)) ATH_MSG_DEBUG("Cluster information cannot be collected!");

  if (pr_testCode){
    if(!testCode()) ATH_MSG_DEBUG("Test code cannot run!");
  }
  
  m_Tree->Fill();

  return StatusCode::SUCCESS;
}
StatusCode EventReaderAlg::testCode(){

          // TILEDIGITS AND TILERAWCHANNEL
    const TileRawChannelContainer* TileRawCnt = nullptr;
    const TileDigitsContainer* TileDigitsCnt = nullptr;

    ATH_CHECK (evtStore()->retrieve (TileDigitsCnt, "TileDigitsCnt"));

    // test max of all kinds of id's
    unsigned int maxAdcHash = m_tileID->adc_hash_max();
    std::cout << "SW MAX CELLS: " << m_tileID->cell_hash_max() << std::endl;
    std::cout << "SW MAX PMT: " << m_tileID->pmt_hash_max() << std::endl;
    std::cout << "SW MAX ADC: " << maxAdcHash << std::endl;

    maxAdcHash = m_tileHWID->adc_hash_max();
    std::cout << "HW MAX CHANNELS: " << m_tileHWID->channel_hash_max() << std::endl;
    std::cout << "HW MAX ADC: " << maxAdcHash << std::endl;

    int maxChannels = m_tileCabling->getMaxChannels();
    int maxGains = m_tileCabling->getMaxGains();
    std::cout << "MAX CHANNELS (in a drawer): " << maxChannels << std::endl;
    std::cout << "MAX GAINS: " << maxGains << std::endl;
    

    for (const TileDigitsCollection* TileDigColl : *TileDigitsCnt){
      if (TileDigColl->empty() ) continue;

      HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
      // int ros = m_tileHWID->ros(adc_id_collection);
      // int drawer = m_tileHWID->drawer(adc_id_collection);
      // int partition = ros - 1;
      // uint32_t rodBCID = TileDigColl->getRODBCID();

      for (const TileDigits* TileDig : *TileDigColl){
        // ATH_MSG_INFO( "  TileDig->samples(): " << TileDig->samples());
        HWIdentifier adc_id = TileDig->adc_HWID(); // adc id, related to hardware. is different from channel id.

        IdentifierHash adc_ch_hash = m_tileHWID->get_channel_hash(adc_id);
        IdentifierHash adc_hash = m_tileHWID->get_hash(adc_id);

        Identifier swcell_id = m_tileCabling->h2s_cell_id(adc_id); // cell id 
        Identifier swpmt_id = m_tileCabling->h2s_pmt_id(adc_id); // cell id 
        Identifier swadc_id = m_tileCabling->h2s_adc_id(adc_id); // cell id 

        // int tileCh = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
        // int tileAdc = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)

        // size_t index = (size_t) (adc_hash);
        // ATH_MSG_INFO ("   adc_id: " << adc_id << ". index: " << index <<". ros: " << ros << ". drawer: " << drawer << ". hw_ch_hash_max (hw_adc_hash_max): " << hw_ch_hash_max << "("<< hw_adc_hash_max << ")" ". rodBCID: " << rodBCID);
        // ATH_MSG_INFO ("   adc_id (ros/drawer/ch/adc): " << adc_id << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" << "index " << index << " adc_ch_hash " << adc_ch_hash << " adc_hash " << adc_hash << " swids (cell/pmt/adc): " << swcell_id << " / " << swpmt_id << " / " << swadc_id <<  ") rodBCID: " << rodBCID);

      } // end loop TileDigitsContainer    
    } //end loop TileDigitsCollection


    ATH_CHECK (evtStore()->retrieve( TileRawCnt , "TileRawChannelCnt"));
    for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
      for (const TileRawChannel* TileChannel : *TileChannelColl){
        // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/TileCalorimeter/TileEvent/TileEvent/TileRawData.h
        int tileIndex, tilePmt = 0;
        Identifier rawCellId = TileChannel->cell_ID();
        Identifier rawPmtId = TileChannel->pmt_ID();
        Identifier rawAdcId = TileChannel->adc_ID();

        HWIdentifier rawAdcHwid = TileChannel->adc_HWID();

        int tileAdc       = m_tileHWID->adc(rawAdcHwid);
        int tileCh   = m_tileHWID->channel(rawAdcHwid);
        int drawer    = m_tileHWID->drawer(rawAdcHwid);
        int ros       = m_tileHWID->ros(rawAdcHwid);

        TileChannel->cell_ID_index(tileIndex,tilePmt);
 
        ATH_MSG_INFO ("TileRawID/rawPmtId/rawAdcId/rawAdcHwid (ros/drawer/ch/adc): " << rawCellId <<"/" << rawPmtId << "/" << rawAdcId << "/" << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << TileChannel->amplitude() << ". Time: " << TileChannel->time()); 

      } //end loop TileRawChannelContainer
    } //end loop TileRawChannelCollection
      
    // ###########################################################################
    // ###########################################################################
    //ALL CALOContainer
    // const CaloCellContainer* CaloCnt = nullptr;
    // ATH_CHECK (evtStore()->retrieve (CaloCnt, "AllCalo"));

    // for (const CaloCell* cell : *CaloCnt){ // loop over the container, that is made of CaloCell's objects
    //   ATH_MSG_INFO ("cellEta " << cell->eta());
    // }

    // #############################################################################
  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpEventInfo(const xAOD::EventInfo *ei){
  // const xAOD::EventInfo *eventInfo = nullptr;
  ATH_CHECK (evtStore()->retrieve (ei, "EventInfo"));

  e_runNumber = ei->runNumber();
  e_eventNumber = ei->eventNumber();
  e_bcid = ei->bcid();

  ATH_MSG_INFO ("in execute, runNumber = " << e_runNumber << typeid(ei->runNumber()).name() << ", eventNumber = " << e_eventNumber << typeid(ei->eventNumber()).name() << ", bcid: " << typeid(e_bcid).name() );
  
  return StatusCode::SUCCESS;
}

int EventReaderAlg::getCaloRegionIndex(const CaloCell* cell){
  if (cell->caloDDE()->is_tile()) return 0; //belongs to Tile
  else if (cell->caloDDE()->is_lar_em()) return 1; //belongs to EM calorimeter
  else if (cell->caloDDE()->is_lar_em_barrel()) return 2; //belongs to EM barrel
  else if (cell->caloDDE()->is_lar_em_endcap()) return 3; //belongs to EM end cap
  else if (cell->caloDDE()->is_lar_em_endcap_inner()) return 4; //belongs to the inner wheel of EM end cap
  else if (cell->caloDDE()->is_lar_em_endcap_outer()) return 5; //belongs to the outer wheel of EM end cap
  else if (cell->caloDDE()->is_lar_hec()) return 6; //belongs to HEC
  else if (cell->caloDDE()->is_lar_fcal()) return 7; //belongs to FCAL
    
  ATH_MSG_ERROR (" #### Region not found for cell offline ID "<< cell->ID() <<" ! Returning -999.");
  return -999; //region not found
}



StatusCode EventReaderAlg::dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName, const LArOnOffIdMapping* larCabling ){
  //Get cable map via read conditions handle
  // SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
  // const LArOnOffIdMapping* larCabling=*larCablingHdl;
  const LArDigitContainer* LarDigCnt = nullptr;
  const TileDigitsContainer* TileDigCnt = nullptr;
  const TileRawChannelContainer* TileRawCnt = nullptr;

  int clusterIndex = 0;

  // calorimeter level
  std::bitset<200000>       larClusteredDigits;
  std::bitset<65536>        tileClusteredDigits;
  std::vector<size_t>       caloHashMap;
  std::vector<HWIdentifier> channelHwidInClusterMap; //unique for any readout in the ATLAS
  std::vector<int>          cellIndexMap;
  std::vector<float>        channelIndexMap;
  // cell level
  std::vector<double>       cellClusDeltaEta;
  std::vector<double>       cellClusDeltaPhi;
  std::vector<double>       cellGranularityEtaInClusterMap;
  std::vector<double>       cellGranularityPhiInClusterMap;
  // channel level
  std::vector<double>       channelEnergyInClusterMap;
  std::vector<double>       channelTimeInClusterMap;
  std::vector<double>       channelEtaInClusterMap;
  std::vector<double>       channelPhiInClusterMap;

  std::vector<bool>         channelCaloRegionMap;
  std::vector<bool>         badChannelMap;
  
  //*****************************************
  
  ATH_CHECK (evtStore()->retrieve ( cls, clsName )); //reading from an ESD file.
  for (const xAOD::CaloCluster* cl : *cls) {

    double clusEta = cl->eta();
    double clusPhi = cl->phi();
    // double cellClusDistPhi = 0.0;

    c_clusterEnergy->push_back(cl->et());
    c_clusterEta->push_back(clusEta);
    c_clusterPhi->push_back(clusPhi);
    c_clusterPt->push_back(cl->pt());
    
    ATH_MSG_INFO ("Cluster: e = " << cl->et() << " , pt = " << cl->pt() << " , eta = " << cl->eta() << " , phi = " << cl->phi());
    ATH_MSG_INFO ("      numberCells : " << cl->numberCells());

    // loop over cells in cluster
    auto itrCells = cl->cell_begin();       
    auto itrCellsEnd = cl->cell_end(); 
    int cellNum = 0; //cell number just for printing out
    int cellIndex = 0; //cell index inside cluster (ordered)
    // int readOutIndex = 0; // channel index inside cluster

      for ( auto itCells=itrCells; itCells != itrCellsEnd; ++itCells){ 
      
        //loop over the cells in a cluster and do stuff...
        const CaloCell* cell = (*itCells);
 
        if (cell) { // check for empty clusters
          double eneCell = cell->energy();
          double timeCell = cell->time();

          double etaCell = cell->eta();
          double phiCell = cell->phi();          
          Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.
          int gainCell = cell->gain(); // CaloGain
          bool badCell = cell->badcell();

          // cell detector descriptors:
          // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloDetDescr/CaloDetDescr/CaloDetDescriptor.h#0021
          int caloRegionIndex = getCaloRegionIndex(cell);
          bool isTile = cell->caloDDE()->is_tile();
          bool isLAr = cell->caloDDE()->is_lar_em();
          bool isLArFwd = cell->caloDDE()->is_lar_fcal();
          bool isLArHEC = cell->caloDDE()->is_lar_hec();
          // bool isLArB = cell->caloDDE()->is_lar_em_barrel();
          // bool isLArEC = cell->caloDDE()->is_lar_em_endcap();
          double deta = cell->caloDDE()->deta(); //delta eta - granularity
          double dphi = cell->caloDDE()->dphi(); //delta phi - granularity
          // cell->caloDDE()->print(); //print out eta, phi and r.
              
          IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
          IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
          IdentifierHash chidHash, adcidHash;
          HWIdentifier chhwid, adchwid;
          size_t index, indexPmt1, indexPmt2;

          if (isTile){ //Tile
            const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
            // ATH_MSG_INFO ("tCell->quality() " << tCell->quality());

            Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

            // main TileCell readout properties:
            int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
            int gain2   = tCell->gain2();
            // int qual1   = tCell->qual1(); // quality factor (chi2)
            // int qual2   = tCell->qual2();
            bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
            bool badch2 = tCell->badch2();
            bool noch1  = (gain1<0 || gain1>1); // there is a ch?
            bool noch2  = (gain2<0 || gain2>1);
            float time1 = tCell->time1(); // reco time in both chs (OF2)
            float time2 = tCell->time2();
            float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
            float ene2  = tCell->ene2();

            // verify if it's a valid ch pmt 1
            if (!noch1 && !badch1){
              // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
              HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
              IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
              indexPmt1 = (size_t) (hashpmt1);

              int adc1     = m_tileHWID->adc(adchwid_pmt1);
              int channel1 = m_tileHWID->channel(adchwid_pmt1);
              int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
              int ros1     = m_tileHWID->ros(adchwid_pmt1);

              // test if there is conflict in the map (only for debugging reason)
              if (tileClusteredDigits.test(indexPmt1)) ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT1 position in PMT map. Position was already filled in this event.");
              else { //If there wasn't an error, 
                tileClusteredDigits.set(indexPmt1); //fill the map.
                caloHashMap.push_back(indexPmt1);
                cellIndexMap.push_back(cellIndex);
                channelIndexMap.push_back(cellIndex + 0.1); // adds 0.1 to the cell index, to indicate this is the PMT 1
                channelHwidInClusterMap.push_back(adchwid_pmt1);
                channelTimeInClusterMap.push_back(time1);
                channelEnergyInClusterMap.push_back(ene1);
                channelCaloRegionMap.push_back(caloRegionIndex);
                badChannelMap.push_back(badch1);

                // readOutIndex++;
              }
              ATH_MSG_INFO (" in IsTile: Cell "<< cellNum <<" ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 << "). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene1 (total)" << ene1 << "(" << eneCell << "). time(final) " << time1 << "(" << timeCell <<"). gain " << gain1 );
            }
            else if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
            else if (badch1)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 channel!");
            else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt1 channel skipped for no known reason.");
            
            // verify if it's a valid ch pmt 2
            if (!noch2 && !badch2){
              HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));            
              IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);            
              indexPmt2 = (size_t) (hashpmt2);

              int adc2     = m_tileHWID->adc(adchwid_pmt2);
              int channel2 = m_tileHWID->channel(adchwid_pmt2);
              int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
              int ros2     = m_tileHWID->ros(adchwid_pmt2);
              
              // test if there is conflict in the map (only for debugging reason)
              if (tileClusteredDigits.test(indexPmt2)) ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event.");
              else {//If there wasn't an error,
                tileClusteredDigits.set(indexPmt2); //fill the map.
                caloHashMap.push_back(indexPmt2);
                cellIndexMap.push_back(cellIndex);
                channelIndexMap.push_back(cellIndex + 0.2); // adds 0.2 to the cell index, to indicate this is the PMT2
                channelHwidInClusterMap.push_back(adchwid_pmt2);
                channelTimeInClusterMap.push_back(time2);
                channelEnergyInClusterMap.push_back(ene2);
                channelCaloRegionMap.push_back(caloRegionIndex);
                badChannelMap.push_back(badch2);

                // readOutIndex++;
              } 
              ATH_MSG_INFO (" in IsTile: Cell "<< cellNum <<" ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 <<  ". ene2(total)" << ene2 << "(" << eneCell << "). time(final) "  << time2 << "(" << timeCell << "). gain " << gain2 );
            }
            else if (noch2)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
            else if (badch2)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt2 channel!");
            else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt2 channel skipped for no known reason.");
            
            // } //end if id
          } // end if isTile

          else if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
            chhwid = larCabling->createSignalChannelID(cellId);
            // std::string hwid_lar = chid.getString();
            // ATH_MSG_INFO (typeid(chhwid).name());
            chidHash =  m_onlineLArID->channel_Hash(chhwid);
            index = (size_t) (chidHash);
            larClusteredDigits.set(index);
            caloHashMap.push_back(index);
            cellIndexMap.push_back(cellIndex);
            channelIndexMap.push_back(cellIndex + 0.0);
            channelHwidInClusterMap.push_back(chhwid);
            channelTimeInClusterMap.push_back(timeCell);
            channelEnergyInClusterMap.push_back(eneCell);
            channelCaloRegionMap.push_back(caloRegionIndex);
            badChannelMap.push_back(badCell);

            // readOutIndex++;

            ATH_MSG_INFO (" in IsLAr: cell "<< cellNum << " ID: " << cellId  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". ene " << eneCell << ". time " << timeCell << ". D_eta: " << deta << ". D_phi: " << dphi << " ):");
          } //end else LAr

          else ATH_MSG_ERROR (" ####### ERROR ! No CaloCell region was found!");

          // ##############################
          //  CELL INFO DUMP
          // ##############################



          // ##############################
          cellIndex++;
          cellNum++;
        } //end if-cell is empty verification
      } // end loop at cells inside cluster

    // ##############################
    //  CLUSTER INFO DUMP
    // ##############################


    // ##############################
    //  CHANNEL INFO DUMP
    // ##############################
    // Loop over ALL channels in LAr Digit Container.
    // Dump only the channels inside the previous clusters
    if (larClusteredDigits.any()){
      ATH_MSG_INFO ("Dumping LAr Digits...");
      ATH_CHECK (evtStore()->retrieve (LarDigCnt, pr_larDigName));

      for (const LArDigit* dig : *LarDigCnt) {
        HWIdentifier channelID = dig->channelID();
        IdentifierHash idHash = m_onlineLArID->channel_Hash(channelID);
        size_t index = (size_t) (idHash);

        if (larClusteredDigits.test(index)) {
          for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){
            if (channelHwidInClusterMap[k]==channelID){
              ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
              // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );
              
            
            // digits
            std::vector<float> larDigit( (dig->samples()).begin(), (dig->samples()).end() ); // get vector conversion from 'short int' to 'float'
            c_larSamples->push_back(dig->samples());
            c_channelDigits->push_back(larDigit);

            // Cell / readout data
            c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
            c_channelTime->push_back(channelTimeInClusterMap[k]);

            // DataDescriptorElements

            // Channel info
            int barrelEc = m_onlineLArID->barrel_ec(channelID);
            int posNeg = m_onlineLArID->pos_neg(channelID);
            int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
            int slot = m_onlineLArID->slot(channelID);
            int chn = m_onlineLArID->channel(channelID);      
            std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, slot, chn } ; //unite info
            c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

            // important indexes
            c_clusterChannelIndex->push_back(channelIndexMap[k]);//each LAr cell is an individual read out.    
            // c_clusterCellIndex->push_back(cellIndexMap[k]); // 
            c_clusterIndex->push_back(clusterIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 

            ATH_MSG_INFO ("In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (short int): " << dig->samples() << ". Dig (float): " << larDigit);
            } //end-if caloHashMap matches
            // else{
            //   if (caloHashMap[k] > m_onlineLArID->channelHashMax() ) ATH_MSG_ERROR ("###### HASH INDEX OUT OF BONDS FOR LAR !...");
            // }
          } //end for loop over caloHashMaps
        } // end-if clustBits Test
      } //end loop over LAr digits container
    } //end-if larClustBits is not empty
    // if(!dumpLArDigits(LarDigCnt,"LArDigitContainer_MC",larClusteredDigits, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("LAr Digits information cannot be collected!");


    if (tileClusteredDigits.any()) {
      ATH_MSG_INFO ("Dumping tile PMT's Digits...");
      // if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
      ATH_CHECK (evtStore()->retrieve (TileDigCnt, pr_tileDigName));
  
      for (const TileDigitsCollection* TileDigColl : *TileDigCnt){
        if (TileDigColl->empty() ) continue;

        HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
        // uint32_t rodBCID = TileDigColl->getRODBCID();

        for (const TileDigits* dig : *TileDigColl){
          HWIdentifier adc_id = dig->adc_HWID();

          IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
          // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
          
          size_t index = (size_t) (adcIdHash);

          if (index <= m_tileHWID->adc_hash_max()){ //safety verification
            if (tileClusteredDigits.test(index)){ //if this channel index is inside cluster map of cells
              // Im aware this may not be the most optimized way to dump the containers info, but the solution below
              // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
              // because the order here is very important to keep the data links.
              for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInClusterMap[k] == adc_id){ //find the index from pmt index to proceed.
                  ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << adc_id );
                  // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );

                  Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc
                  
                  // Digits     
                  c_tileSamples->push_back(dig->samples()); //temporarily maintained
                  c_channelDigits->push_back(dig->samples());

                  // Channel / readout data
                  c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
                  c_channelTime->push_back(channelTimeInClusterMap[k]);
                  

                  // cell detector descriptors:
                  //(https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloDetDescr/CaloDetDescr/CaloDetDescriptor.h#0021)
                  // double eneCell = cell->energy();
                  // double etaCell = cell->eta();
                  // double phiCell = cell->phi();
                  // double timeCell = cell->time();
                  // Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.
                  // int gainCell = cell->gain();             
                  // bool isTile = cell->caloDDE()->is_tile();
                  // bool isLArB = cell->caloDDE()->is_lar_em_barrel();
                  // bool isLArEC = cell->caloDDE()->is_lar_em_endcap();
                  // bool isLArHEC = cell->caloDDE()->is_lar_hec();
                  // double deta = cell->caloDDE()->deta(); //delta eta
                  // double dphi = cell->caloDDE()->dphi(); //delta phi
                  // // cell->caloDDE()->print(); //print out eta, phi and r.
                  // c_is_tile->push_back(isTile);
                  // c_is_lar_em_barrel->push_back(isLArB);
                  // c_is_lar_em_endcap->push_back(isLArEC);
                  // c_is_lar_hec->push_back(isLArHEC);
                  // c_cellDEta->push_back(deta);
                  // c_cellDPhi->push_back(dphi);
                  // c_cellEta->push_back(etaCell);
                  // c_cellPhi->push_back(phiCell);
                  // c_channelEnergy->push_back(eneCell);
                  // c_channelTime->push_back(timeCell);
                  // // c_cellMap->push_back(hwid_lar);
                  // c_cellGain->push_back(gainCell);
                  // c_channelBad->push_back(cell->badcell());

                  //channelInfo
                  int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
                  int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
                  int partition = ros - 1; //partition (0-3)
                  int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
                  int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
                  std::vector<int> chInfo{partition, drawer, tileChNumber, tileAdcGain}; //unite info

                  // c_tileChannel->push_back(tileChNumber); // xxxxxxxxxxxx excluir
                  // c_tileDrawer->push_back(drawer); // xxxxxxxxxxxx excluir
                  // c_tilePartition->push_back(partition); // xxxxxxxxxxxx excluir
                  c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  // important indexes
                  c_clusterChannelIndex->push_back(channelIndexMap[k]);
                  // c_clusterCellIndex->push_back(cellIndexMap[k]);
                  c_clusterIndex->push_back(clusterIndex); // what roi this cell belongs
                  // c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
                  // c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING
                  

                  ATH_MSG_INFO ("In DumpTile Digits:" << " ReadOutIndex: " << channelIndexMap[k] << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());   

                  // readOutIndex++;
                  // cellIndex++;
                } // end if-pmt index comparison
                // if (caloHashMap[k] > m_tileHWID->adc_hash_max()){
                //   ATH_MSG_ERROR (" ###### HASH INDEX OUT OF BONDS FOR TILE !...");
                // } //end else
              }// end loop over pmt-indexes
            } // end if digit index exist on clusteredDigits
          } //end if hash exists
        } // end loop TileDigitsCollection
      } //end loop TileDigitsContainer
    } // end DumpTileDigits
    
    clusterIndex++;

    larClusteredDigits.reset();
    tileClusteredDigits.reset();

    caloHashMap.clear();
  
    channelHwidInClusterMap.clear();
    cellIndexMap.clear();
    channelIndexMap.clear();
    channelEnergyInClusterMap.clear();
    channelTimeInClusterMap.clear();
    channelEtaInClusterMap.clear();
    channelPhiInClusterMap.clear();
    cellGranularityEtaInClusterMap.clear();
    cellGranularityPhiInClusterMap.clear();
    channelCaloRegionMap.clear();
    badChannelMap.clear();
    cellClusDeltaEta.clear();
    cellClusDeltaPhi.clear();
  } // end loop at clusters inside container

  return StatusCode::SUCCESS;
}



StatusCode  EventReaderAlg::dumpTileRawCh(const TileRawChannelContainer *rawChannelContainer, const std::string& rawChannelName, std::bitset<65536>& clustBits){

  ATH_CHECK (evtStore()->retrieve( rawChannelContainer , rawChannelName));

  for (const TileRawChannelCollection* TileChannelColl : *rawChannelContainer){
    for (const TileRawChannel* TileChannel : *TileChannelColl){
      
      Identifier rawCellId = TileChannel->cell_ID();
      Identifier rawPmtId = TileChannel->pmt_ID();
      Identifier rawAdcId = TileChannel->adc_ID();

      HWIdentifier rawAdcHwid = TileChannel->adc_HWID();

      IdentifierHash chHwidHash = m_tileHWID->get_channel_hash(rawAdcHwid);

      size_t index = (size_t) (chHwidHash);

      int tileIndex, tilePmt = 0;
      int tileAdc       = m_tileHWID->adc(rawAdcHwid);
      int tileCh   = m_tileHWID->channel(rawAdcHwid);
      int drawer    = m_tileHWID->drawer(rawAdcHwid);
      int ros       = m_tileHWID->ros(rawAdcHwid);

      TileChannel->cell_ID_index(tileIndex,tilePmt);
      // if (rawChDupletHash.any()){ //if has a duplet
      //   if (clustBits.test(index)){ //if this channel index is inside 
          // std::vector<float> rawChDuplet = {0.0, 0.0};
            // rawChDupletEne[pmtIndex] = TileChannel->amplitude();
      

        ATH_MSG_INFO ("In DumpTile Raw: TileRawID/rawPmtId/rawAdcId/rawAdcHwid (ros/drawer/ch/adc): " << rawCellId <<"/" << rawPmtId << "/" << rawAdcId << "/" << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << TileChannel->amplitude() << ". Time: " << TileChannel->time()); 
      //   } // end-if clust.test
      // } // end-if has a duplet

    } //end loop TileRawChannelContainer
  } //end loop TileRawChannelCollection

  return StatusCode::SUCCESS;

}

StatusCode EventReaderAlg::dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg){
  ATH_CHECK (evtStore()->retrieve (jets, jetAlg )); //get the jet container from file in evt store

  for (const xAOD::Jet* jet : *jets){ //loop over jets in container
    ATH_MSG_INFO ("Jet pt: " << jet->pt() << " MeV" << ". JetType: " << jet->type());


  } //end loop over jets in container
  return StatusCode::SUCCESS;
}

void EventReaderAlg::bookBranches(TTree *tree){
  // Event info
  tree->Branch ("RunNumber", &e_runNumber);
  tree->Branch ("EventNumber", &e_eventNumber);
  tree->Branch ("BCID", &e_bcid);

  // ## Cluster 
  tree->Branch ("cluster_et",&c_clusterEnergy);
  tree->Branch ("cluster_pt",&c_clusterPt);
  tree->Branch ("cluster_eta",&c_clusterEta);
  tree->Branch ("cluster_phi",&c_clusterPhi);
  tree->Branch ("cluster_index",&c_clusterIndex);

  // Cluster cell
  tree->Branch ("cluster_cell_eta",&c_cellEta);
  tree->Branch ("cluster_cell_phi",&c_cellPhi);
  tree->Branch ("cluster_cell_deta",&c_cellDEta);
  tree->Branch ("cluster_cell_dphi",&c_cellDPhi);
  tree->Branch ("cluster_cellsDist_dphi",&c_cellToClusterDPhi);
  tree->Branch ("cluster_cellsDist_deta",&c_cellToClusterDEta);
  tree->Branch ("cluster_cell_index",&c_clusterCellIndex);
  tree->Branch ("cluster_cell_caloGain",&c_cellGain);
  
  
  // Cluster channel
  tree->Branch ("cluster_channel_index",&c_clusterChannelIndex);
  tree->Branch ("cluster_channel_digits",&c_channelDigits);
  tree->Branch ("cluster_channel_energy",&c_channelEnergy);
  tree->Branch ("cluster_channel_time",&c_channelTime);
  tree->Branch ("cluster_channel_bad",&c_channelBad);  

  tree->Branch ("cluster_channel_chInfo", &c_channelChInfo); //modify
  tree->Branch ("cluster_cell_caloGain",&c_cellGain); //modify
  tree->Branch ("cluster_hwid_cellMap",&c_cellMap); // modify (cannot be hwid because this library only exists inside Athena)

  
  // to be removed....
  tree->Branch ("cluster_cell_larSamples",&c_larSamples);
  tree->Branch ("cluster_cell_tileSamples",&c_tileSamples);  
  // tree->Branch ("cluster_cells_isTile",&c_is_tile);
  // tree->Branch ("cluster_cells_tileChannel",&c_tileChannel);
  // tree->Branch ("cluster_cells_tileDrawer",&c_tileDrawer);
  // tree->Branch ("cluster_cells_tilePartition",&c_tilePartition);
  // tree->Branch ("cluster_cells_isLArEmBarrel",&c_is_lar_em_barrel);
  // tree->Branch ("cluster_cells_isLArEMEC",&c_is_lar_em_endcap);
  // tree->Branch ("cluster_cells_isLArHEC",&c_is_lar_hec);

  
  
  // Jet cells info
  // tree->Branch ("jet_et",&j_jetEt);

  // tree->Branch ("cluster_cell_id",&c_cellIdentifier);
}

void EventReaderAlg::clear(){
  // Event info
  e_runNumber = 9999;
  e_eventNumber = 9999;
  e_bcid = 9999;

  // ## Cluster 
  c_clusterEnergy->clear();
  c_clusterPt->clear();
  c_clusterEta->clear();
  c_clusterPhi->clear();
  c_clusterIndex->clear();

  // Cluster cell
  c_cellEta->clear();
  c_cellPhi->clear();
  c_cellDEta->clear();
  c_cellDPhi->clear();
  c_cellToClusterDPhi->clear();
  c_cellToClusterDEta->clear();
  c_clusterCellIndex->clear();
  c_cellGain->clear();
  
  // Cluster channel
  c_clusterChannelIndex->clear();
  c_channelDigits->clear();
  c_channelEnergy->clear();
  c_channelTime->clear();
  c_channelBad->clear();

  c_channelChInfo->clear(); //modify
  c_cellGain->clear(); //modify
  c_cellMap->clear(); // modify (cannot be hwid because this library only exists inside Athena)

  
  // to be removed....
  c_larSamples->clear();
  c_tileSamples->clear();
  // tree->Branch ("cluster_cells_isTile",&c_is_tile);
  // tree->Branch ("cluster_cells_tileChannel",&c_tileChannel);
  // tree->Branch ("cluster_cells_tileDrawer",&c_tileDrawer);
  // tree->Branch ("cluster_cells_tilePartition",&c_tilePartition);
  // tree->Branch ("cluster_cells_isLArEmBarrel",&c_is_lar_em_barrel);
  // tree->Branch ("cluster_cells_isLArEMEC",&c_is_lar_em_endcap);
  // tree->Branch ("cluster_cells_isLArHEC",&c_is_lar_hec);

  // e_runNumber = 9999;
  // e_eventNumber = 9999;
  // e_bcid = 9999;

  // c_channelDigits->clear();
  // // c_digitsCluster->clear();
  // c_clusterCellIndex->clear();
  // c_clusterIndex->clear();
  // c_clusterChannelIndex->clear();
  // c_channelChInfo->clear();
  // c_larSamples->clear();
  // c_tileSamples->clear();
  // c_channelEnergy->clear();
  // c_channelTime->clear();
  // c_cellEta->clear();
  // c_cellPhi->clear();
  // c_cellDEta->clear();
  // c_cellDPhi->clear();
  // c_cellToClusterDPhi->clear();
  // c_cellToClusterDEta->clear();
  // c_is_tile->clear();
  // c_tileChannel->clear();
  // c_tileDrawer->clear();
  // c_tilePartition->clear();
  // c_is_lar_em_barrel->clear();
  // c_channelBad->clear();
  // c_cellGain->clear();
  // c_clusterEnergy->clear();
  // c_clusterEta->clear();
  // c_clusterPhi->clear();
  // c_clusterPt->clear();
  // c_is_lar_em_endcap->clear();
  // c_is_lar_hec->clear();
  // c_cellMap->clear();
}

StatusCode EventReaderAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}

// StatusCode EventReaderAlg::dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName, const LArOnOffIdMapping* larCabling ){
//   //Get cable map via read conditions handle
//   // SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
//   // const LArOnOffIdMapping* larCabling=*larCablingHdl;
//   const LArDigitContainer* LarDigCnt = nullptr;
//   const TileDigitsContainer* TileDigCnt = nullptr;
//   const TileRawChannelContainer* TileRawCnt = nullptr;

//   int clusterIndex = 0;
//   //*****************************************
  
//   ATH_CHECK (evtStore()->retrieve ( cls, clsName )); //reading from an ESD file.
//   for (const xAOD::CaloCluster* cl : *cls) {

//     std::bitset<200000> larClusteredDigits;
//     std::bitset<65536> tileClusteredDigits;
//     std::vector<size_t> tileHashMap;

//     double clusEta = cl->eta();
//     double clusPhi = cl->phi();
//     std::vector<double> cellClusDistEta;
//     std::vector<double> cellClusDistPhi;
//     // double cellClusDistPhi = 0.0;

//     c_clusterEnergy->push_back(cl->et());
//     c_clusterEta->push_back(clusEta);
//     c_clusterPhi->push_back(clusPhi);
//     c_clusterPt->push_back(cl->pt());
    
//     ATH_MSG_INFO ("Cluster: e = " << cl->et() << " , pt = " << cl->pt() << " , eta = " << cl->eta() << " , phi = " << cl->phi());
//     ATH_MSG_INFO ("      numberCells : " << cl->numberCells());

//     // loop over cells in cluster
//     auto itrCells = cl->cell_begin();       
//     auto itrCellsEnd = cl->cell_end(); 
//     int cellNum = 0;
//     int cellIndex = 0;
//     int readOutIndex = 0;

//       for ( auto itCells=itrCells; itCells != itrCellsEnd; ++itCells){ 
      
//         //loop over the cells in a cluster and do stuff...
//         const CaloCell* cell = (*itCells);
 
//         if ( !getCaloCellInfo(cell, tileClusteredDigits, tileHashMap, larClusteredDigits, cellNum, larCabling) ) ATH_MSG_INFO (" Could not get CaloCellInfo !");
//         // getCaloCellInfo(cell, tileClusteredDigits, tileClusteredDigits_pmt2, larClusteredDigits, cellIndex, larCabling);


//         cellNum++;
//       } // end loop at cells inside cluster


//     // Loop over ALL cells in LAr Digit Container.
//     // Dump only the cells inside the previous clusters
//     if (larClusteredDigits.any()){
//       ATH_MSG_INFO ("Dumping LAr...");
//       if(!dumpLArDigits(LarDigCnt,"LArDigitContainer_MC",larClusteredDigits, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("LAr Digits information cannot be collected!");
//     }

//     if (tileClusteredDigits.any()) {
//       ATH_MSG_INFO ("Dumping tile PMT's...");
//       if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
//     }
    
//     // if (tileClusteredDigits.any()) {
//     //   ATH_MSG_INFO ("Dumping tile Raw Ch...")
//     //   if(!dumpTileRawCh(TileRawCnt,"TileRawChannelCnt",tileClusteredDigits))
//     // }
//     for (auto ci : *c_cellEta){
//       cellClusDistEta.push_back(clusEta - ci);
//     } 
//     for (auto cj : *c_cellPhi){
//       cellClusDistPhi.push_back(clusPhi - cj);
//     } 
//     // c_cellToClusterDEta->push_back(cellClusDistEta);
//     // c_cellToClusterDPhi->push_back(cellClusDistPhi);

//     // c_clusterIndex->push_back(clusterIndex);
//     // c_digitsCluster->push_back(c_channelDigits);
//     clusterIndex++;
//   } // end loop at clusters inside container

//   return StatusCode::SUCCESS;
// }

// StatusCode EventReaderAlg::getCaloCellInfo(const CaloCell* cell, std::bitset<65536>& clustBitsTile, std::vector<size_t>& tileHashMap, std::bitset<200000>& clustBitsLAr, int cellNum, const LArOnOffIdMapping* larCabling){
//   if (cell) { // check for empty clusters
//     double eneCell = cell->energy();
//     double etaCell = cell->eta();
//     double phiCell = cell->phi();
//     double timeCell = cell->time();
//     Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.
//     int gainCell = cell->gain();

//     // cell detector descriptors:
//     // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloDetDescr/CaloDetDescr/CaloDetDescriptor.h#0021
//     bool isTile = cell->caloDDE()->is_tile();
//     bool isLArB = cell->caloDDE()->is_lar_em_barrel();
//     bool isLArEC = cell->caloDDE()->is_lar_em_endcap();
//     bool isLArHEC = cell->caloDDE()->is_lar_hec();
//     double deta = cell->caloDDE()->deta(); //delta eta
//     double dphi = cell->caloDDE()->dphi(); //delta phi
//     // cell->caloDDE()->print(); //print out eta, phi and r.
        
//     IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
//     IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
//     IdentifierHash chidHash, adcidHash;
//     HWIdentifier chhwid, adchwid;
//     size_t index, indexPmt1, indexPmt2;
    

//     if (isTile){ //Tile
//       const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
//       // ATH_MSG_INFO ("tCell->quality() " << tCell->quality());

//       Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

//       // main TileCell readout properties:
//       int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
//       int gain2   = tCell->gain2();
//       // int qual1   = tCell->qual1(); // quality factor (chi2)
//       // int qual2   = tCell->qual2();
//       bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
//       bool badch2 = tCell->badch2();
//       bool noch1  = (gain1<0 || gain1>1); // there is a ch?
//       bool noch2  = (gain2<0 || gain2>1);
//       float time1 = tCell->time1(); // reco time in both chs (OF2)
//       float time2 = tCell->time2();
//       float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
//       float ene2  = tCell->ene2();

//       // verify if it's a valid ch pmt 1
//       if (!noch1 && !badch1){
//         // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
//         HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
//         IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
//         indexPmt1 = (size_t) (hashpmt1);

//         int adc1     = m_tileHWID->adc(adchwid_pmt1);
//         int channel1 = m_tileHWID->channel(adchwid_pmt1);
//         int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
//         int ros1     = m_tileHWID->ros(adchwid_pmt1);

//         // test if there is conflict in the map (only for debugging reason)
//         if (clustBitsTile.test(indexPmt1)) ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT1 position in PMT map. Position was already filled in this event.");
//         else { //If there wasn't an error, 
//           clustBitsTile.set(indexPmt1); //fill the map.
//           tileHashMap.push_back(indexPmt1);
//         }
//         ATH_MSG_INFO ("in IsTile: Cell "<< cellNum <<" ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 <<"). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene " << ene1 << ". time " << time1 << ". gain " << gain1 );
//       }
//       else if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
//       else if (badch1)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 channel!");
//       else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt1 channel skipped for no known reason.");
      
//       // verify if it's a valid ch pmt 2
//       if (!noch2 && !badch2){
//         HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));            
//         IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);            
//         indexPmt2 = (size_t) (hashpmt2);

//         int adc2     = m_tileHWID->adc(adchwid_pmt2);
//         int channel2 = m_tileHWID->channel(adchwid_pmt2);
//         int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
//         int ros2     = m_tileHWID->ros(adchwid_pmt2);
        
//         // test if there is conflict in the map (only for debugging reason)
//         if (clustBitsTile.test(indexPmt2)) ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event.");
//         else {//If there wasn't an error,
//           clustBitsTile.set(indexPmt2); //fill the map.
//           tileHashMap.push_back(indexPmt2);
//         } 
//         ATH_MSG_INFO ("in IsTile: Cell "<< cellNum <<" ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 << ". ene " << ene2 << ". time " << time2 << ". gain " << gain2 );
//       }
//       else if (noch2)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
//       else if (badch2)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt2 channel!");
//       else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt2 channel skipped for no known reason.");
      
//       // } //end if id
//     } // end if isTile

//     else { //LAr
//       chhwid = larCabling->createSignalChannelID(cellId);
//       // std::string hwid_lar = chid.getString();
//       // ATH_MSG_INFO (typeid(chhwid).name());
//       chidHash =  m_onlineLArID->channel_Hash(chhwid);
//       index = (size_t) (chidHash);
//       clustBitsLAr.set(index);

//       ATH_MSG_INFO (" in IsLAr: cell "<< cellNum << " ID: " << cellId  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". is_lar_em_barrel? " << isLArB << ". D_eta: " << deta << ". D_phi: " << dphi << " ):");
//     } //end else LAr

//     // THIS PART IS NOT ORDERED WITH THE DUMP MAP !!!
//     // c_is_tile->push_back(isTile);
//     // c_is_lar_em_barrel->push_back(isLArB);
//     // c_is_lar_em_endcap->push_back(isLArEC);
//     // c_is_lar_hec->push_back(isLArHEC);
//     // c_cellDEta->push_back(deta);
//     // c_cellDPhi->push_back(dphi);
//     // c_cellEta->push_back(etaCell);
//     // c_cellPhi->push_back(phiCell);
//     // c_channelEnergy->push_back(eneCell);
//     // c_channelTime->push_back(timeCell);
//     // // c_cellMap->push_back(hwid_lar);
//     // c_cellGain->push_back(gainCell);
//     // c_channelBad->push_back(cell->badcell());
  
//     } // end if cluster is not empty
//   return StatusCode::SUCCESS;
// } 

// StatusCode EventReaderAlg::dumpLArDigits(const LArDigitContainer *digitsContainer, const std::string& digitsContName, std::bitset<200000>& clustBits, int& cellIndex, int& readOutIndex, int& roiIndex){
//   ATH_CHECK (evtStore()->retrieve (digitsContainer, digitsContName));

//   for (const LArDigit* dig : *digitsContainer) {
//     HWIdentifier channelID = dig->channelID();
//     IdentifierHash idHash = m_onlineLArID->channel_Hash(channelID);
//     size_t index = (size_t) (idHash);

//     if (clustBits.test(index)) {
      
//       // digits
//       std::vector<float> larDigit( (dig->samples()).begin(), (dig->samples()).end() ); // get vector conversion from 'short int' to 'float'
//       c_larSamples->push_back(dig->samples());
//       c_channelDigits->push_back(larDigit);

//       // DataDescriptorElements

//       // Channel info
//       int barrelEc = m_onlineLArID->barrel_ec(channelID);
//       int posNeg = m_onlineLArID->pos_neg(channelID);
//       int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
//       int slot = m_onlineLArID->slot(channelID);
//       int chn = m_onlineLArID->channel(channelID);      
//       std::vector<int> chInfo ; //unite info
//       c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

//       // important indexes
//       c_clusterChannelIndex->push_back(readOutIndex);//each LAr cell is an individual read out.    
//       c_clusterCellIndex->push_back(cellIndex); //MUST CORRECT THIS, NOT WORKING  
//       c_clusterIndex->push_back(roiIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 

//       ATH_MSG_INFO ("In DumpLAr Digits: ReadOutIndex "<< readOutIndex  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (short int): " << dig->samples() << ". Dig (float): " << larDigit);
//     } //end if digit obj is inside cluster cells
//   } //end loop over digit container

//   //clear de bitset before the next cluster in container
//   // clustBits.reset();

//   return StatusCode::SUCCESS;
// }

// StatusCode EventReaderAlg::dumpTileDigits(const TileDigitsContainer *digitsContainer, const std::string& digitsContName, std::bitset<65536>& clustBits, std::vector<size_t>& tileHashMap, int& cellIndex, int& readOutIndex, int& roiIndex){
//   ATH_CHECK (evtStore()->retrieve (digitsContainer, digitsContName));
  
//   for (const TileDigitsCollection* TileDigColl : *digitsContainer){
//     if (TileDigColl->empty() ) continue;

//     HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
//     // uint32_t rodBCID = TileDigColl->getRODBCID();

//     for (const TileDigits* dig : *TileDigColl){
//       HWIdentifier adc_id = dig->adc_HWID();

//       IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
//       // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
      
//       size_t index = (size_t) (adcIdHash);

//       if (index <= m_tileHWID->adc_hash_max()){ //safety verification: 
//         if (clustBits.test(index)){ //if this channel index is inside cluster map of cells
//           // Im aware this may not be the most optimized way to dump the containers info, but the solution below
//           // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
//           // because the order here is very important to keep the data links.
//           for (unsigned int k=0 ; k < tileHashMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
//             if (tileHashMap[k] == index){ //find the index from pmt index to proceed.
//               ATH_MSG_INFO ("Hey, the pmtIndex == index " << tileHashMap[k] << "[" << k << "]" << "=" << index );

//               Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc
              

//               // Digits     
//               c_tileSamples->push_back(dig->samples()); //temporarily maintained
//               c_channelDigits->push_back(dig->samples());

//               // cell detector descriptors:
//               //(https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloDetDescr/CaloDetDescr/CaloDetDescriptor.h#0021)
//               // double eneCell = cell->energy();
//               // double etaCell = cell->eta();
//               // double phiCell = cell->phi();
//               // double timeCell = cell->time();
//               // Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.
//               // int gainCell = cell->gain();             
//               // bool isTile = cell->caloDDE()->is_tile();
//               // bool isLArB = cell->caloDDE()->is_lar_em_barrel();
//               // bool isLArEC = cell->caloDDE()->is_lar_em_endcap();
//               // bool isLArHEC = cell->caloDDE()->is_lar_hec();
//               // double deta = cell->caloDDE()->deta(); //delta eta
//               // double dphi = cell->caloDDE()->dphi(); //delta phi
//               // // cell->caloDDE()->print(); //print out eta, phi and r.xxxxxxxxx
//               // c_is_tile->push_back(isTile); xxxxxxxxx             
//               // c_is_lar_em_barrel->push_back(isLArB);xxxxxxxxxxx
//               // c_is_lar_em_endcap->push_back(isLArEC);xxxxxxxxxxx
//               // c_is_lar_hec->push_back(isLArHEC);xxxxxxxxxxxx
//               // c_cellDEta->push_back(deta);
//               // c_cellDPhi->push_back(dphi);
//               // c_cellEta->push_back(etaCell);
//               // c_cellPhi->push_back(phiCell);
//               // c_channelEnergy->push_back(eneCell);
//               // c_channelTime->push_back(timeCell);xxxxxxxxx
//               // // c_cellMap->push_back(hwid_lar);
//               // c_cellGain->push_back(gainCell);
//               // c_channelBad->push_back(cell->badcell());

//               //channelInfo
//               int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
//               int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
//               int partition = ros - 1; //partition (0-3)
//               int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
//               int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
//               c_tileChannel->push_back(tileChNumber);
//               c_tileDrawer->push_back(drawer);
//               c_tilePartition->push_back(partition);
//               std::vector<int> chInfo{partition, drawer, tileChNumber}; //unite info
//               c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

//               // important indexes
//               c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
//               c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING
//               c_clusterIndex->push_back(roiIndex); // what roi this cell belongs

//               ATH_MSG_INFO ("In DumpTile Digits:" << " ReadOutIndex: " << readOutIndex << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());   

//               readOutIndex++;
//               cellIndex++;
//             } // end if-pmt index comparison
//             else{
//               ATH_MSG_INFO (" still looking...");
//             } //end else
//           }// end loop over pmt-indexes
//         } // end if digit index exist on clusteredDigits
//       } //end if hash exists
//     } // end loop TileDigitsContainer    
//   } //end loop TileDigitsCollection

//   return StatusCode::SUCCESS;
// }


// athena.py EventReader_jobOptions.py > reader.log 2>&1; code reader.log
// athena.py EventReader_jobOptions.py > reader_test.log 2>&1; code reader_test.log
// cd ../build/;make -j;source x86*/setup*;cd ../run/
// cp ../source/EventReader/share/EventReader_jobOptions.py ../run/EventReader_jobOptions.py
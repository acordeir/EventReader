#ifndef EVENTREADER_EVENTREADERALG_H
#define EVENTREADER_EVENTREADERALG_H
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "AthenaBaseComps/AthAlgorithm.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/JetContainer.h"
#include "LArRawEvent/LArDigitContainer.h"
#include "LArRawEvent/LArDigit.h"
#include "LArRawEvent/LArRawChannelContainer.h"
#include "TileEvent/TileDigitsContainer.h"
#include "TileEvent/TileDigits.h"
#include "TileEvent/TileDigitsCollection.h"
#include "TileEvent/TileRawChannelContainer.h"
#include "TileEvent/TileRawChannelCollection.h"
#include "TileEvent/TileRawChannel.h"
#include "TileEvent/TileCell.h"


#include "Identifier/Identifier.h"
#include "Identifier/HWIdentifier.h"
#include "LArIdentifier/LArOnlineID.h"
#include "LArCabling/LArOnOffIdMapping.h"
// #include "TileConditions/TileCablingService.h"
#include "TileConditions/TileCablingSvc.h"
#include "CaloIdentifier/CaloCell_ID.h"
// #include "CaloIdentifier/TileHWID.h"
// #include "CaloIdentifier/TileID.h"

#include <TH1.h>
#include <TTree.h>

#include <bitset>

// #include "CaloEvent/CaloCell.h"
// #include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
// #include "xAODTrigRinger/TrigRingerRingsContainer.h"
// #include <xAODCaloRings/versions/RingSet_v1.h>
// #include <xAODCaloRings/RingSetContainer.h>
// #include <xAODEgamma/versions/Electron_v1.h>
// #include <xAODEgamma/ElectronContainer.h>
// #include "xAODTruth/TruthParticle.h"
// #include "xAODTruth/TruthParticleContainer.h"
// #include "xAODTruth/xAODTruthHelpers.h"
// #include <xAODCaloRings/versions/CaloRings_v1.h>
// #include <xAODCaloRings/CaloRingsContainer.h>


// using namespace std;
// class LArDigitContainer;
class LArOnOffIdMapping;
class TileCablingSvc;
// class TileOnOffIdMapping;

class EventReaderAlg: public ::AthAlgorithm 
{ 
// The structure here is built to separate the implementation from de header file. 
// So, the implementation goes in another file, a *.cxx one. 
 public: 
    EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~EventReaderAlg(); 
    virtual StatusCode  initialize() override;     //once, before any input is loaded
    virtual StatusCode  execute() override;        //per event
    virtual StatusCode  finalize() override;       //once, after all events processed
    void                clear();
    void                bookBranches(TTree *tree);
    int                 getCaloRegionIndex(const CaloCell* cell); //customized representative indexes of calo region
    virtual StatusCode  dumpEventInfo(const xAOD::EventInfo *ei);
    virtual StatusCode  dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName, const LArOnOffIdMapping* larCabling);
    virtual StatusCode  dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg);
    // virtual StatusCode  dumpLArDigits(const LArDigitContainer *digitsContainer, const std::string& digitsContName, std::bitset<200000>& clustBits, int& cellIndex, int& readOutIndex, int& roiIndex);
    // virtual StatusCode  dumpTileDigits(const TileDigitsContainer *digitsContainer, const std::string& digitsContName, std::bitset<65536>& clustBits, std::vector<size_t>& tileHashMap, int& cellIndex, int& readOutIndex, int& roiIndex);
    virtual StatusCode  dumpTileRawCh(const TileRawChannelContainer *rawChannelContainer, const std::string& rawChannelName, std::bitset<65536>& clustBits); 
    // virtual StatusCode  getCaloCellInfo(const CaloCell* cell, std::bitset<65536>& clustBitsTile, std::vector<size_t>& tileHashMap, std::bitset<200000>& clustBitsLAr, int cellNum, const LArOnOffIdMapping* larCabling);
    virtual StatusCode  testCode();

  private: 
    Gaudi::Property<std::string> pr_clusterName {this, "clusterName" , "CaloCalTopoClusters" ,  "Name of the cluster container that will have its cells dumped."};
    Gaudi::Property<std::string> pr_jetName {this, "jetName" , "AntiKt4EMPFlowJets" ,  "Name of the jet container that will have its cells dumped."};
    Gaudi::Property<std::string> pr_tileDigName {this, "tileDigName" , "TileDigitsCnt" ,  "Name of the tile digits container that will have its values dumped."};
    Gaudi::Property<std::string> pr_larDigName {this, "larDigName" , "LArDigitContainer_MC" ,  "Name of the LAr digits container that will have its values dumped."};    
    Gaudi::Property<bool> pr_doTile {this, "doTile", true, "Dump the tile cells inside any ROI."}; //pending
    Gaudi::Property<bool> pr_noBadCells {this, "noBadCells", false, "Skip the cells tagged as badCells."}; //pending
    Gaudi::Property<bool> pr_doLAr {this, "doLAr", true, "Dump the LAr cells inside any ROI."}; //pending
    Gaudi::Property<bool> pr_printCells {this, "printCells", true, "Print out the cells basic info during the dump."}; //pending
    Gaudi::Property<bool> pr_testCode {this, "testCode", false, "Execute testing code function."};

    ServiceHandle<ITHistSvc> m_ntsvc;
    ServiceHandle<TileCablingSvc> m_tileCablingSvc { this, "TileCablingSvc", "TileCablingSvc", "Tile cabling service"}; //Name of Tile cabling service
    SG::ReadCondHandleKey<LArOnOffIdMapping> m_larCablingKey;
    // SG::ReadCondHandleKey<LArOnOffIdMapping> m_cablingKey{this,"CablingKey","LArOnOffIdMap","SG Key of LArOnOffIdMapping object"};

    // const LArOnOffIdMapping* larCabling=*larCablingHdl;
    const LArOnlineID* m_onlineLArID; //from detector store LAr
    const TileHWID* m_tileHWID;//Atlas detector Identifier for Tile Calorimeter online (hardware) identifiers
    const TileID* m_tileID; //Atlas detector Identifier class for Tile Calorimeter offline identifiers
    const TileCablingService* m_tileCabling;
    const CaloCell_ID* m_calocell_id;

    TTree *m_Tree;

    // ## EventInfo ##
    unsigned int e_runNumber = 9999; ///< Run number
    unsigned int e_bcid = 9999; ///< BCID number
    unsigned long long e_eventNumber = 9999; ///< Event number
    //##################

    // ## Cluster ##
    std::vector < double > *c_clusterEnergy = nullptr; //energy of the cluster
    std::vector < double > *c_clusterEta = nullptr; // clus. baricenter eta
    std::vector < double > *c_clusterPhi = nullptr; // clus. baricenter phi
    std::vector < double > *c_clusterPt = nullptr; // clus. baricenter eta
    // Cell
    std::vector < int > *c_clusterCellIndex = nullptr; // cell index inside a cluster
    std::vector < int > *c_cellGain = nullptr; // gain of cell signal, from 'CaloGain' standard.
    std::vector < double > *c_cellEta = nullptr; // cell inside cluster baricenter eta
    std::vector < double > *c_cellPhi = nullptr; // cell inside cluster baricenter phi
    std::vector < double > *c_cellDEta = nullptr; // cell inside cluster delta_eta (granularity)
    std::vector < double > *c_cellDPhi = nullptr; // cell inside cluster delta_phi (granularity)
    std::vector < double > *c_cellToClusterDPhi = nullptr; // cell inside cluster phi distance to cluster baricenter.
    std::vector < double > *c_cellToClusterDEta = nullptr; // cell inside cluster eta distance to cluster baricenter.
    // Channel
    // CRIAR CHANNEL GAIN OU ADD IN chInfo: 
    std::vector < int > *c_clusterIndex = nullptr; // cluster index, for each channel index. (they have the same number of inputs)
    std::vector < float > *c_clusterChannelIndex = nullptr; // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
    std::vector < std::vector < int > > *c_channelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel)
    std::vector < std::vector < float > > *c_channelDigits = nullptr;  // samples from LAr and Tile cells/channels
    std::vector < std::vector < short > > *c_larSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < std::vector < float > > *c_tileSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < double > *c_channelEnergy = nullptr; // energy of cell or readout channel inside cluster
    std::vector < double > *c_channelTime = nullptr; // time of cell inside cluster
    std::vector < bool > *c_channelBad = nullptr; // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
    
    // std::vector < std::vector < float > > *c_bothCaloSamples = nullptr; //samples of tile and LAr cells inside a cluster
    std::vector < std::string > *c_cellMap = nullptr; //cell map of ALL cells inside a cluster
    
    std::vector < unsigned long long > *c_cellIdentifier = nullptr; //id 0x2d214a140000000. is a 64-bit number that represent the cell.
    std::vector < bool > *c_is_tile = nullptr; // xxxxxxxxxxxxxxxxxxxx excluir
    std::vector < bool > *c_is_lar_em_barrel = nullptr; // xxxxxxxxxxxxxxxxxxxx excluir
    std::vector < bool > *c_is_lar_em_endcap = nullptr; // xxxxxxxxxxxxxxxxxxxx excluir
    std::vector < bool > *c_is_lar_hec = nullptr; // xxxxxxxxxxxxxxxxxxxx excluir
    std::vector < int > *c_tileChannel = nullptr; // xxxxxxxxxxxxxx excluir
    std::vector < int > *c_tileDrawer = nullptr; // xxxxxxxxxxxxxx excluir
    std::vector < int > *c_tilePartition = nullptr; // xxxxxxxxxxxxxx excluir
    //##################
    
}; 


#endif 

#ifndef EVENTREADER_EVENTREADERALG_H
#define EVENTREADER_EVENTREADERALG_H
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "AthenaBaseComps/AthAlgorithm.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/JetContainer.h"
#include "LArRawEvent/LArDigitContainer.h"
#include "LArRawEvent/LArDigit.h"

#include "LArIdentifier/LArOnlineID.h"
#include "Identifier/Identifier.h"
#include "Identifier/HWIdentifier.h"

#include <TH1.h>
#include <TTree.h>

// #include "CaloEvent/CaloCell.h"
// #include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
// #include "xAODTrigRinger/TrigRingerRingsContainer.h"
// #include <xAODCaloRings/versions/RingSet_v1.h>
// #include <xAODCaloRings/RingSetContainer.h>
// #include <xAODEgamma/versions/Electron_v1.h>
// #include <xAODEgamma/ElectronContainer.h>
// #include "xAODTruth/TruthParticle.h"
// #include "xAODTruth/TruthParticleContainer.h"
// #include "xAODTruth/xAODTruthHelpers.h"
// #include <xAODCaloRings/versions/CaloRings_v1.h>
// #include <xAODCaloRings/CaloRingsContainer.h>


// using namespace std;
// class LArDigitContainer;
class LArOnOffIdMapping;

class EventReaderAlg: public ::AthAlgorithm 
{ 
// The structure here is built to separate the implementation from de header file. 
// So, the implementation goes in another file, a *.cxx one. 
 public: 
    EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~EventReaderAlg(); 
    virtual StatusCode  initialize() override;     //once, before any input is loaded
    virtual StatusCode  execute() override;        //per event
    virtual StatusCode  finalize() override;       //once, after all events processed
    void                clear();
    void                bookBranches(TTree *tree);
    virtual StatusCode  dumpEventInfo(const xAOD::EventInfo *ei);
    virtual StatusCode  dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName);
    virtual StatusCode  dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg);
    virtual StatusCode  dumpJetRoiInfo();
  private: 
    Gaudi::Property<std::string> pr_clusterName {this, "clusterName" , "CaloCalTopoClusters" ,  "Name of the cluster container that will have its cells dumped."};
    Gaudi::Property<std::string> pr_jetName {this, "jetName" , "AntiKt4EMPFlowJets" ,  "Name of the jet container that will have its cells dumped."};
    Gaudi::Property<float> pr_Roi_r {this, "Roi_r" , 0 ,  "Radius of the jets region of interest."};

    ServiceHandle<ITHistSvc> m_ntsvc;
    SG::ReadCondHandleKey<LArOnOffIdMapping> m_larCablingKey;    
    const LArOnlineID* m_onlineID;

    TTree *m_Tree;

    unsigned int e_runNumber = 9999; ///< Run number
    unsigned int e_bcid = 9999; ///< BCID number
    unsigned long long e_eventNumber = 9999; ///< Event number
   // float j_pt_cut = 20.0; 
    std::vector < std::vector < short > > *c_larSamples = nullptr; //samples of LAr cells inside a cluster
    std::vector < double > *c_clusterEnergy = nullptr; //energy of the cluster
    std::vector < double > *c_clusterEta = nullptr; // clus. baricenter eta
    std::vector < double > *c_clusterPhi = nullptr; // clus. baricenter phi
    std::vector < double > *c_clusterPt = nullptr; // clus. baricenter eta
    std::vector < double > *c_cellEnergy = nullptr; // energy of cell inside cluster
    std::vector < double > *c_cellTime = nullptr; // time of cell inside cluster
    std::vector < double > *c_cellEta = nullptr; // cell inside cluster baricenter eta 
    std::vector < double > *c_cellPhi = nullptr; // cell inside cluster baricenter phi
    std::vector < double > *c_cellDEta = nullptr; // cell inside cluster delta_eta
    std::vector < double > *c_cellDPhi = nullptr; // cell inside cluster delta_phi
    std::vector < float > *j_jet_pt = nullptr; // transverse momentum of jets 
    std::vector < float > *j_eta = nullptr; // vector to store jets eta 
    std::vector < float > *j_phi = nullptr; // vector to store jets phi
    std::vector < unsigned long long > *c_cellIdentifier = nullptr; //id 0x2d214a140000000. is a 64-bit number that represent the cell.
    std::vector < bool > *c_is_tile = nullptr; // is this cell inside cluster, on tileCal region?
    std::vector < bool > *c_is_lar_em_barrel = nullptr; // is this cell inside cluster, on LAr EM Barrel region?
    std::vector < bool > *c_is_lar_em_endcap = nullptr; // is this cell inside cluster, on LAr EM EndCap region?
    std::vector < bool > *c_is_lar_hec = nullptr; // is this cell inside cluster, on LAr Hadronic EndCap region?
    std::vector < bool > *c_badCell = nullptr; // is this cell dead?
    std::vector < int > *c_cellGain = nullptr; // gain of cell signal
    // enum CaloGain {
    //     TILELOWLOW =-16 ,
    //     TILELOWHIGH =-15 ,
    //     TILEHIGHLOW  = -12,
    //     TILEHIGHHIGH = -11,
    //     TILEONELOW   =-4,
    //     TILEONEHIGH  =-3,
    //     INVALIDGAIN = -1, 
    //     LARHIGHGAIN = 0, 
    //     LARMEDIUMGAIN = 1,  
    //     LARLOWGAIN = 2,
    //     LARNGAIN =3,
    //     UNKNOWNGAIN=4};
  
    
}; 


#endif //> !RINGERDUMPER_RINGERDUMPERALG_H

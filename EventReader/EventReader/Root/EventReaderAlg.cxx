/// PhotonTruthMatchingTool includes
#include "EventReader/EventReaderAlg.h"
#include <xAODEventInfo/EventInfo.h>

// CaloCell

#include "CaloEvent/CaloCell.h"
#include "CaloEvent/CaloCompactCell.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloEvent/CaloCompactCellContainer.h"
#include "CaloEvent/CaloClusterCellLink.h"
#include "CaloEvent/CaloClusterCellLinkContainer.h"
// #include "LArRawChannelContainer/"

// Readout lib's/afs/cern.ch/user/a/acordeir/private/event_reader/EventReader/EventReader/Root
#include "LArCabling/LArOnOffIdMapping.h"


#include <bitset>

// #include <xAODJet/JetContainer.h>



EventReaderAlg::EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator ) :
    AthAlgorithm( name, pSvcLocator )
    , m_ntsvc("THistSvc/THistSvc", name)
    , m_larCablingKey("LArOnOffIdMap")
    , m_onlineID(0)
    // , m_ntsvc("THistSvc/THistSvc", name)
    {

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("LArOnOffMap",m_larCablingKey);

}

EventReaderAlg::~EventReaderAlg() {
  // delete m_runNumber;
  // delete m_bcid;
  // delete m_eventNumber;

}

StatusCode EventReaderAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  ATH_CHECK( detStore()->retrieve(m_onlineID, "LArOnlineID") );
  ATH_CHECK( m_larCablingKey.initialize());

  // Book the variables to save in the *.root
  m_Tree = new TTree("data", "data");
  bookBranches(m_Tree);
  if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [data]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  clear();

  const xAOD::EventInfo *eventInfo = nullptr;
  const xAOD::CaloClusterContainer* cls = nullptr;
  const xAOD::JetContainer* jets = nullptr;

  // EventInfo
//  if(!dumpEventInfo(eventInfo)) ATH_MSG_DEBUG("Event information cannot be collected!");
  // Clusters
//  if(!dumpClusterInfo(cls,pr_clusterName)) ATH_MSG_DEBUG("Cluster information cannot be collected!");
  // Jets
  if(!dumpJetsInfo(jets,pr_jetName)) ATH_MSG_DEBUG("Jets information cannot be collected!");
  if(!dumpJetRoiInfo()) ATH_MSG_DEBUG("Digits information cannot be collected!");

  m_Tree->Fill();

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpEventInfo(const xAOD::EventInfo *ei){
  // const xAOD::EventInfo *eventInfo = nullptr;
  ATH_CHECK (evtStore()->retrieve (ei, "EventInfo"));

  e_runNumber = ei->runNumber();
  e_eventNumber = ei->eventNumber();
  e_bcid = ei->bcid();

  ATH_MSG_INFO ("in execute, runNumber = " << e_runNumber << typeid(ei->runNumber()).name() << ", eventNumber = " << e_eventNumber << typeid(ei->eventNumber()).name() << ", bcid: " << typeid(e_bcid).name() );

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName ){
  //*****************************************
  std::bitset<200000> clusteredDigits;
  //Get cable map via read conditions handle
  SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
  const LArOnOffIdMapping* larCabling=*larCablingHdl;
  //*****************************************

  ATH_CHECK (evtStore()->retrieve ( cls, clsName )); //reading from an ESD file.
  for (const xAOD::CaloCluster* cl : *cls) {

    // float ePS = 0.0;
    // float eEM1 = 0.0;
    // float eEM2 = 0.0;
    // float eEM3 = 0.0;

    // ePS = cl->energyBE(0);
    // eEM1 = cl->energyBE(1);
    // eEM2 = cl->energyBE(2);
    // eEM3 = cl->energyBE(3);

    c_clusterEnergy->push_back(cl->et());
    c_clusterEta->push_back(cl->eta());
    c_clusterPhi->push_back(cl->phi());
    c_clusterPt->push_back(cl->pt());

    // ATH_MSG_INFO("Cluster:   ePS  : " << ePS << ".     eEM1: " << eEM1 << ".     eEM2: " << eEM2 << ".     eEM3: " << eEM3);
    // ATH_MSG_INFO("      numberCells : " << cl->numberCells());
    ATH_MSG_INFO ("Cluster: e = " << cl->et() << " , pt = " << cl->pt() << " , eta = " << cl->eta() << " , phi = " << cl->phi());


      // loop over cells in cluster
      auto itrCells = cl->cell_begin();
      auto itrCellsEnd = cl->cell_end();
      unsigned cellNum = 0;

      for ( auto itCells=itrCells; itCells != itrCellsEnd; ++itCells){

        //loop over the cells in a cluster and do stuff...
        const CaloCell* cell = (*itCells);

        if (cell) { // check for empty clusters
          double eneCell = cell->energy();
          double etaCell = cell->eta();
          double phiCell = cell->phi();
          double timeCell = cell->time();
          Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.
          int gainCell = cell->gain();

          // cell detector descriptors:
          // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloDetDescr/CaloDetDescr/CaloDetDescriptor.h#0021
          bool isTile = cell->caloDDE()->is_tile();
          bool isLArB = cell->caloDDE()->is_lar_em_barrel();
          bool isLArEC = cell->caloDDE()->is_lar_em_endcap();
          bool isLArHEC = cell->caloDDE()->is_lar_hec();
          double deta = cell->caloDDE()->deta(); //delta eta
          double dphi = cell->caloDDE()->dphi(); //delta phi
          // cell->caloDDE()->print(); //print out eta, phi and r.

          //*********************************************************
           // Based on 'LArDigitThinnerFromEMClust':
          HWIdentifier hwid = larCabling->createSignalChannelID(cellId);
          IdentifierHash idHash =  m_onlineID->channel_Hash(hwid);
          size_t index = (size_t) (idHash);
          clusteredDigits.set(index);

          //*********************************************************

          ATH_MSG_INFO ("cell "<< cellNum << " (id " << cellId <<". Gain: " << gainCell <<  "  is_tile? " << isTile << ". is_lar_em_barrel? " << isLArB << ". is_lar_em_endcap? " << isLArEC << ". is_lar_hec? " << isLArHEC <<". D_eta: " << deta << ". D_phi: " << dphi << ". Index: " << index <<" ):");
          // ATH_MSG_INFO ("cell "<< cellNum << " (id " << cellId << ". idHash " << hwid << ". Index: " << index << ".  is_tile? " << isTile << ". is_lar_em_barrel? " << isLArB << ". D_eta: " << deta << ". D_phi: " << dphi << " ):");
          // ATH_MSG_INFO ("Cell energy: " << eneCell << ". Time: "<< timeCell << ". Eta: " << etaCell << ". Phi: "<< phiCell );
          // hist("h_etaCellsCluster")->Fill( etaCell );

          c_is_tile->push_back(isTile);
          c_is_lar_em_barrel->push_back(isLArB);
          c_is_lar_em_endcap->push_back(isLArEC);
          c_is_lar_hec->push_back(isLArHEC);
          c_cellDEta->push_back(deta);
          c_cellDPhi->push_back(dphi);
          c_cellEta->push_back(etaCell);
          c_cellPhi->push_back(phiCell);
          c_cellEnergy->push_back(eneCell);
          c_cellTime->push_back(timeCell);
          // c_cellIdentifier->push_back(cellId);
          c_cellGain->push_back(gainCell);




          cellNum++;
        } // end if 'cell' verification
      } // end loop at cells inside cluster
  } // end loop at clusters inside container

  // Loop over ALL cells in LAr Digit Container.
  // Dump only the cells inside the previous clusters
  const LArDigitContainer* LarDigCnt = nullptr;
  ATH_CHECK (evtStore()->retrieve (LarDigCnt, "LArDigitContainer_MC"));

  for (const LArDigit* dig : *LarDigCnt) {
    HWIdentifier channelID = dig->channelID();
    IdentifierHash idHash = m_onlineID->channel_Hash(channelID);
    size_t index = (size_t) (idHash);
    if (clusteredDigits.test(index)) {
      // m_larDigitsTrimmed->push_back(dig);
      c_larSamples->push_back(dig->samples());
      // ATH_MSG_INFO (" Samples: " << dig->samples() << typeid((dig->samples()) ).name());
    } //end if digit obj is inside cluster cells
  } //end loop over digit container

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg){
  ATH_CHECK (evtStore()->retrieve (jets, jetAlg )); //get the jet container from file in evt store
  ATH_MSG_INFO("Number of jets: " <<  jets->size());
  for (const xAOD::Jet* jet : *jets){ //loop over jets in container
    ATH_MSG_INFO ("Jet pt: " << jet->pt() << " MeV" << "Eta; Phi : " << jet->eta() << ";" << jet->phi());
    j_jet_pt->push_back(jet->pt());
    j_eta->push_back(jet->eta());
    j_phi->push_back(jet->phi());
  } //end loop over jets in container
  return StatusCode::SUCCESS;
}


StatusCode EventReaderAlg::dumpJetRoiInfo(){
 //*****************************************
  std::bitset<200000> JetROIDigits;
 // Get cable map via read conditions handle
  SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
  const LArOnOffIdMapping* larCabling=*larCablingHdl;
 //*****************************************
  const CaloCellContainer* CaloCnt = nullptr;
  float r = pr_Roi_r;
  ATH_CHECK (evtStore()->retrieve (CaloCnt, "AllCalo"));
  if (j_jet_pt->empty()==0){
    ATH_MSG_INFO("Entering cells loop");
    //loop over cells for each jet in event
    for (int i = 0; i < j_eta->size(); ++i) {
      for (const CaloCell* cell : *CaloCnt ){
        //checking if cell is in jet's ROI
        if (cell->eta()<=j_eta[0][i]+r && cell->eta()>=j_eta[0][i]-r){
          if (cell->phi()<=j_phi[0][i]+r && cell->phi()>=j_phi[0][i]-r){
            // Based on 'LArDigitThinnerFromEMClust':
            HWIdentifier hwid = larCabling->createSignalChannelID(cell->ID());
            IdentifierHash idHash =  m_onlineID->channel_Hash(hwid);
            size_t index = (size_t) (idHash);
            JetROIDigits.set(index);
          }
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}


          // c_cellIdentifier->push_back(cellId);
//  const LArDigitContainer* LarDigCnt = nullptr;
//  ATH_CHECK (evtStore()->retrieve (LarDigCnt, "LArDigitContainer_MC"));

//  for (const LArDigit* dig : *LarDigCnt) {
//    ATH_MSG_INFO ("eta CELL DIGITS: " << dig->eta())
    //HWIdentifier channelID = dig->channelID();
    //IdentifierHash idHash = m_onlineID->channel_Hash(channelID);
    //size_t index = (size_t) (idHash);
    //if (clusteredDigits.test(index)) {
      // m_larDigitsTrimmed->push_back(dig);
     // c_larSamples->push_back(dig->samples());
      // ATH_MSG_INFO (" Samples: " << dig->samples() << typeid((dig->samples()) ).name());
     //end if digit obj is inside cluster cells
//  } //end loop over digit container

 // return StatusCode::SUCCESS;
//}

void EventReaderAlg::bookBranches(TTree *tree){
  // Event info
  tree->Branch ("RunNumber", &e_runNumber);
  tree->Branch ("EventNumber", &e_eventNumber);
  tree->Branch ("BCID", &e_bcid);
  // Cluster cells info
  tree->Branch ("cluster_et",&c_clusterEnergy);
  tree->Branch ("cluster_pt",&c_clusterPt);
  tree->Branch ("cluster_eta",&c_clusterEta);
  tree->Branch ("cluster_phi",&c_clusterPhi);
  tree->Branch ("cluster_cells_larSamples",&c_larSamples);
  tree->Branch ("cluster_cells_gain",&c_cellGain);
  tree->Branch ("cluster_cells_energy",&c_cellEnergy);
  tree->Branch ("cluster_cells_time",&c_cellTime);
  tree->Branch ("cluster_cells_eta",&c_cellEta);
  tree->Branch ("cluster_cells_phi",&c_cellPhi);
  tree->Branch ("cluster_cells_deta",&c_cellDEta);
  tree->Branch ("cluster_cells_dphi",&c_cellDPhi);
  tree->Branch ("cluster_cells_isTile",&c_is_tile);
  tree->Branch ("cluster_cells_isLArEmBarrel",&c_is_lar_em_barrel);
  tree->Branch ("cluster_cells_isLArEMEC",&c_is_lar_em_endcap);
  tree->Branch ("cluster_cells_isLArHEC",&c_is_lar_hec);
  tree->Branch ("cluster_badCell",&c_badCell);
  // Jet cells info
  tree->Branch ("jet_pt",&j_jet_pt);
  tree->Branch ("jet_eta",&j_eta);
  tree->Branch ("jet_phi",&j_phi);
  // tree->Branch ("cluster_cell_id",&c_cellIdentifier);



}

void EventReaderAlg::clear(){
  e_runNumber = 9999;
  e_eventNumber = 9999;
  e_bcid = 9999;

  c_larSamples->clear();
  c_cellEnergy->clear();
  c_cellTime->clear();
  c_cellEta->clear();
  c_cellPhi->clear();
  c_cellDEta->clear();
  c_cellDPhi->clear();
  c_is_tile->clear();
  c_is_lar_em_barrel->clear();
  c_badCell->clear();
  c_cellGain->clear();
  c_clusterEnergy->clear();
  c_clusterEta->clear();
  c_clusterPhi->clear();
  c_clusterPt->clear();
  c_is_lar_em_endcap->clear();
  c_is_lar_hec->clear();
 // c_cellIdentifier->clear();
  j_jet_pt->clear();
  j_eta->clear();
  j_phi->clear();
}

StatusCode EventReaderAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////////
// StatusCode EventReaderAlg::execute() {
//   ATH_MSG_DEBUG ("Executing " << name() << "...");
// //   setFilterPassed(false); //optional: start with algorithm not passed
// //   if (!collectInfo()) ATH_MSG_DEBUG("Unable to collect info. Please check input file");
// //   m_Tree->Fill();
//   const xAOD::EventInfo *eventInfo = nullptr;
//   ATH_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
//   // cl->retrieveMoment("choose moment","where it will be stored")
//   ATH_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber() << ", bcid: " << eventInfo->bcid());

//   const xAOD::CaloClusterContainer* cls = nullptr;
//   ATH_CHECK (evtStore()->retrieve ( cls, "CaloCalTopoClusters" ));
//   // ATH_CHECK (evtStore()->retrieve ( cls, "egammaClusters" ));
//   // ATH_CHECK (evtStore()->retrieve ( cls, "egamma711Clusters" ));
//   // ATH_CHECK (evtStore()->retrieve ( cls, "TauPi0Clusters" ));
//   for (const xAOD::CaloCluster* cl : *cls) {

//     // double moment = 0.0;
//     float etaPS = 0.0;
//     float etaEM1 = 0.0;
//     float etaEM2 = 0.0;
//     float etaEM3 = 0.0;
//     float phiPS = 0.0;
//     float phiEM1 = 0.0;
//     float phiEM2 = 0.0;
//     float phiEM3 = 0.0;
//     float ePS = 0.0;
//     float eEM1 = 0.0;
//     float eEM2 = 0.0;
//     float eEM3 = 0.0;
//     //https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODCaloEvent/xAODCaloEvent/versions/CaloCluster_v1.h#0187
//     etaPS = cl->etaBE(0); // layer PS
//     etaEM1 = cl->etaBE(1); // layer EM1
//     etaEM2 = cl->etaBE(2); // layer EM2
//     etaEM3 = cl->etaBE(3); // layer EM3
//     phiPS = cl->phiBE(0); // layer PS
//     phiEM1 = cl->phiBE(1); // layer EM1
//     phiEM2 = cl->phiBE(2); // layer EM2
//     phiEM3 = cl->phiBE(3); // layer EM3
//     ePS = cl->energyBE(0);
//     eEM1 = cl->energyBE(1);
//     eEM2 = cl->energyBE(2);
//     eEM3 = cl->energyBE(3);
//     // cl->retrieveMoment( xAOD::CaloCluster::PHICALOFRAME, moment );
//     ATH_MSG_INFO("  Cluster etaPS: " << etaPS << ". etaEM1: " << etaEM1 << ". etaEM2: " << etaEM2 << ". etaEM3: " << etaEM3);
//     ATH_MSG_INFO("          phiPS: " << phiPS << ". phiEM1: " << phiEM1 << ". phiEM2: " << phiEM2 << ". phiEM3: " << phiEM3);
//     ATH_MSG_INFO("          ePS  : " << ePS << ".     eEM1: " << eEM1 << ".     eEM2: " << eEM2 << ".     eEM3: " << eEM3);
//     ATH_MSG_INFO("          cluster size: " << cl->clusterSize() << ". etaSize: "<< cl->getClusterEtaSize() << ". phiSize: "<< cl->getClusterPhiSize() << ". cellNumber: " << cl->numberCells());


//     // for ( auto it=itrCCont; it != itrCContEnd; ++it){
//     //   // 'itrCCont' is an iterator of CaloClusterContainer, that points to its Clusters. It is a pointer (CaloCluter) to a pointer (CaloCell)
//     //   // ' *(itrCCont) is the pointer to the CaloCell.
//     //   const xAOD::CaloCluster* testClus = (*it);
//     //   ATH_MSG_INFO ("const xAOD::CaloCluster* testClus = (*itrCCont);");
//     //   if (testClus){
//     //     // ATH_MSG_INFO ("if (testClus){. (*itrCCont)->cell_begin() is of type: " <<  typeid((*itrCCont)->cell_begin()).name() << ". itrCCont is of type: " << typeid(itrCCont).name() );
//     //     // initial range of cells in a Cluster. The de-referenced CaloCluster is a CaloCell pointer.
//       // auto itrClus = cl->cell_begin();

//     //     // final range of cells in a Cluster. The de-referenced CaloCluster is a CaloCell
//       // auto itrClusEnd = cl->cell_end();


//     // xAOD::CaloCluster::const_cell_iterator cellIter = cl->cell_begin(); //TA CRASHANDO AQUI!!
//     // xAOD::CaloCluster::const_cell_iterator cellIter = nullptr;//cl->cell_begin(); //TA CRASHANDO AQUI!!
//     // unsigned cellNum = 0;
//     // ATH_MSG_INFO ("Loop cell begin");
//     // for( ;cellIter!=cellIterEnd;cellIter++) {
//     //   ATH_MSG_INFO ("Inside loop. Cell obj. creation...");
//     //   const CaloCell* cell = (*cellIter);
//     //   ATH_MSG_INFO ("OK! Good! Let's verify if the cell is valid/empty...");
//     //   if (cell) { // check for empty clusters
//     //       // double eneCell = cell->energy();
//     //       // ATH_MSG_DEBUG ("                    Cell energy: " << eneCell);
//     //       ATH_MSG_DEBUG ("        cell "<< cellNum);
//     //       cellNum++;
//     //     //
//     //   }
//     // }
//   }

//   // xAOD::CaloClusterContainer::const_iterator itrCCont = cls->begin(); //the initial range (or cluster) of the cluster container
//   // xAOD::CaloClusterContainer::const_iterator itrCContEnd  = cls->end();  // the final range (or cluster) of the cluster container
//   // // loop over the clusters
//   // int nCellsClus = 0;
//   // for ( auto it=itrCCont; it != itrCContEnd; ++it){
//   //   // 'itrCCont' is an iterator of CaloClusterContainer, that points to its Clusters. It is a pointer (CaloCluter) to a pointer (CaloCell)
//   //   // ' *(itrCCont) is the pointer to the CaloCell.
//   //   const xAOD::CaloCluster* testClus = (*it);
//   //   ATH_MSG_INFO ("const xAOD::CaloCluster* testClus = (*itrCCont);");
//   //   if (testClus){
//   //     // ATH_MSG_INFO ("if (testClus){. (*itrCCont)->cell_begin() is of type: " <<  typeid((*itrCCont)->cell_begin()).name() << ". itrCCont is of type: " << typeid(itrCCont).name() );
//   //     // initial range of cells in a Cluster. The de-referenced CaloCluster is a CaloCell pointer.
//   //     auto itrClus = (*it)->cell_begin();

//   //     // final range of cells in a Cluster. The de-referenced CaloCluster is a CaloCell
//   //     auto itrClusEnd = (*it)->cell_end();
//   //     //  xAOD::CaloCluster::const_cell_iterator
//   //   }
//   // }

//   return StatusCode::SUCCESS;
// }



// athena.py EventReader_jobOptionsNew.py > reader.log 2>&1; code reader.log
// cd ../build/;make;source x86*/setup*;cd ../run/

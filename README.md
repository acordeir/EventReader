




TileCal readout system
element     range       meaning
-------     -----       -------

ros         1 to 4      ReadOutSystem number ( 1,2 = pos/neg Barrel (side A/C)
                                                3,4 = pos/neg Ext.Barrel (side A/C) )
drawer      0 to 63     64 drawers (modules) in one cylinder (phi-slices)
channel     0 to 47     channel number in the drawer
adc         0 to 1      ADC number for the channel (0 = low gain, 1 = high gain)


Possible Gain values
    enum CaloGain {
        TILELOWLOW =-16 ,
        TILELOWHIGH =-15 ,
        TILEHIGHLOW  = -12,
        TILEHIGHHIGH = -11,
        TILEONELOW   =-4,
        TILEONEHIGH  =-3,
        INVALIDGAIN = -1, 
        LARHIGHGAIN = 0, 
        LARMEDIUMGAIN = 1,  
        LARLOWGAIN = 2,
        LARNGAIN =3,
        UNKNOWNGAIN=4};